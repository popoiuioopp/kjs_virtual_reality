﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ChangeMaterials : MonoBehaviour
{

    public GameObject Object;
    public GameObject Object1;
    public GameObject Object2;
    public Material[] myMaterials = new Material[3];
    public string filename2 = "Assets/CSV Files/Latest.csv";
    public int MatNum;

    void Start()
    {
        //Object = GameObject.FindWithTag("SkinBow");
        string[] Material = takeOutput(@filename2);
        string Mat = Material[0];
        string Last = Mat.Substring(Math.Max(0, Mat.Length - 1));
        MatNum = Int16.Parse(Last);
        Object.GetComponent<SkinnedMeshRenderer>().material = myMaterials[MatNum - 1];
        Object1.GetComponent<SkinnedMeshRenderer>().material = myMaterials[MatNum - 1];
        Object2.GetComponent<SkinnedMeshRenderer>().material = myMaterials[MatNum - 1];
        //Debug.Log("A new scene was loaded");
    }

    private void Update()
    {
        //Object = GameObject.FindWithTag("SkinBow");
        //Object.GetComponent<SkinnedMeshRenderer>().material = myMaterials[MatNum - 1];
    }

    public static string[] takeOutput(string filepath)
    {
        string[] notFound = { "Not Found" };

        try
        {
            string[] lines = System.IO.File.ReadAllLines(@filepath);
            return lines;
        }

        catch (Exception ex)
        {
            return notFound;
            throw new ApplicationException("This program did an oopsie :", ex);
        }
    }
}
