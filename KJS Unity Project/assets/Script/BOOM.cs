﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class BOOM : MonoBehaviour
{
    public float delay = 3f;
    public float radius = 5f;
    public float force = 700f;
    float countdown;
    private Interactable interactable;

    public GameObject explosionEffect;

    bool hasExploded = false;

    public void OnCollisionEnter(Collision collision)
    {
        if (interactable != null && interactable.attachedToHand != null) //don't explode in hand
            return;
        if (collision.collider.tag == "Ground") 
           { 
                Explode();
                FindObjectOfType<AudioManager>().Play("Bomb1");
        }
    }
    public void Explode()
    {
        GameObject boom = Instantiate(explosionEffect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        
        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            ArcheryTarget dest = nearbyObject.GetComponent<ArcheryTarget>();
            if (rb != null && dest != null)
            {
                rb.AddExplosionForce(force, transform.position, radius);
                dest.Destroy_hit();
                //dest.Destroy_enemy2();
            }

           /* ArcheryTarget dest = nearbyObject.GetComponent<ArcheryTarget>();
            if (dest != null)
            {
                Debug.Log("before Destroy hit");
                dest.Destroy_hit();
            }*/
        }

        Destroy(gameObject);
        Destroy(boom, 2f);
    }
}
