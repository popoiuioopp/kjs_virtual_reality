﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Training_Manager : MonoBehaviour
{
    public GameObject enemy_1_object = null;
    public GameObject enemy_2_object = null;
    public GameObject enemy_3_object = null;
    public Transform spawnPoint1;
    public Transform spawnPoint2;
    public Transform spawnPoint3;

    private bool enemy1_bool = false;
    private bool enemy2_bool = false;
    private bool enemy3_bool = false;

    Transform position1;
    Transform position2;
    Transform position3;
    private void Start()
    {
        position1 = spawnPoint1;
        position2 = spawnPoint2;
        position3 = spawnPoint3;
    }

    private void Update()
    {

        if(GameObject.FindGameObjectsWithTag("Enemy1").Length <= 0 && enemy1_bool == false)
        {
            
            StartCoroutine(WaitForSec(enemy_1_object, position1, enemy1_bool));
            enemy1_bool = true;
        }

        if (GameObject.FindGameObjectsWithTag("Enemy2").Length <= 0 && enemy2_bool == false)
        {
            
            StartCoroutine(WaitForSec2(enemy_2_object, position2, enemy2_bool));
            enemy2_bool = true;
        }

        if (GameObject.FindGameObjectsWithTag("Enemy3").Length <= 0 && enemy3_bool == false)
        {

            StartCoroutine(WaitForSec3(enemy_3_object, position3, enemy3_bool));
            enemy3_bool = true;
        }

    }

    IEnumerator WaitForSec(GameObject enemy,Transform spawnPoint, bool enemyCheck)
    {
        yield return new WaitForSeconds(3);   
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        enemy1_bool = false;
    }

    IEnumerator WaitForSec2(GameObject enemy, Transform spawnPoint, bool enemyCheck)
    {
        yield return new WaitForSeconds(3);
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        enemy2_bool = false;
    }

    IEnumerator WaitForSec3(GameObject enemy, Transform spawnPoint, bool enemyCheck)
    {
        yield return new WaitForSeconds(3);
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        enemy3_bool = false;
    }



}
