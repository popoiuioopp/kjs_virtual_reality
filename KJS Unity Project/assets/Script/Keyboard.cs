﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Keyboard : MonoBehaviour
{
    string word = "";
    public TMPro.TextMeshProUGUI name;
    public TMPro.TextMeshProUGUI ask;

    public GameObject upperText;
    public GameObject lowerText;

    public GameObject IntoPart;
    public GameObject TypePart;

    public bool upper = true;
    public bool intro = true;

    void Start()
    {
        string playerfile = "Assets/CSV Files/Playername.csv";

        try
        {
            string[] nameList = System.IO.File.ReadAllLines(@playerfile);
            string previousName = nameList[0];

            ask.text = "(Your current name: " + previousName + ")";
        }

        catch (Exception ex)
        {
            ask.text = "(Your current name: User1)";
            //throw new ApplicationException("This program did an oopsie :", ex);
            Debug.LogError("This program did an oopsie :" + ex.Message);
        }
    }

    public void noChange()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void alphabetFunction(string letter)
    {
        word = word + letter;
        name.text = word + "|";
    }

    public void deleteFunction()
    {
        if (word.Length == 0)
        {
            name.text = "|";
        }
        else
        {
            word = word.Remove(word.Length - 1);
            name.text = word + "|";
        }
    }

    public void capFunction()
    {
        if (upper)
        {
            upper = false;
            upperText.SetActive(false);
            lowerText.SetActive(true);

        } else if (!upper)
        {
            upper = true;
            upperText.SetActive(true);
            lowerText.SetActive(false);
        }
    }

    public void enterFunction()
    {
        string playerfile = "Assets/CSV Files/Playername.csv";
        System.IO.File.WriteAllText(@playerfile, string.Empty);

        try
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@playerfile, true))
            {
                if (word == "" || word == "|")
                {
                    file.WriteLine("User1");
                }
                else
                {
                    file.WriteLine(word);
                }
            }
        }

        catch (Exception ex)
        {
            //throw new ApplicationException("This program did an oopsie :", ex);
            Debug.LogError("This program did an oopsie :" + ex.Message);
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); 

    }

    public void introSwitch()
    {
        if (intro)
        {
            intro = false;
            IntoPart.SetActive(false);
            TypePart.SetActive(true);

        }
        else if (!intro)
        {
            intro = true;
            IntoPart.SetActive(true);
            TypePart.SetActive(false);
        }
    }

    public void test()
    {
        Debug.Log("TEST");
    }
}
