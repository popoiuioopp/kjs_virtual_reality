﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StreakUI : MonoBehaviour
{

	public Slider slider;
	public Gradient gradient;
	public Image fill;
	public Text text;

	private void Update()
	{
		text.text = GameManager.streak + "x";
	}

	public void SetMaxTime(float time)
	{
		slider.maxValue = time;
		slider.value = time;

		fill.color = gradient.Evaluate(1f);
	}

	public void SetTime(float time)
	{
		slider.value = time;

		fill.color = gradient.Evaluate(slider.normalizedValue);
	}

}