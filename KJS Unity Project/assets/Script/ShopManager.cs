﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

public class ShopManager : MonoBehaviour
{
    public GameObject moneytext;
    public GameObject telltext;

    public string filename1 = "Assets/CSV Files/Items.csv";
    public string filename2 = "Assets/CSV Files/Latest.csv";
    public string filename3 = "Assets/CSV Files/ItemLists.csv";
    public string moneyFile = "Assets/CSV Files/Money.csv";

    public int real_money;

    void Start()
    {
        string[] money_str = System.IO.File.ReadAllLines(moneyFile);
        real_money = Int16.Parse(money_str[0]);
    }

    public void Update()
    {
        moneytext.GetComponent<Text>().text = "Coin: " + real_money.ToString("F0");
    }

    public void clearFile()
    {
        System.IO.File.WriteAllText(@filename1, string.Empty);
        System.IO.File.WriteAllText(@filename2, string.Empty);
        addRecord("Item 1", filename1);
        addRecord("Item 1", filename2);
        addRecord("1", filename2);
        addRecord("1", filename2);
        telltext.GetComponent<Text>().text = "";
        updateMoney(20, moneyFile);
        real_money = 20;
        Debug.Log("clear");
    }

    public void OnClicked(string item)
    {
        //string item = button.name;
        //Debug.Log(item);

        if (readRecord(item, filename1))
        {
            // update data in csv 2
            string[] lines = System.IO.File.ReadAllLines(@filename2); // read data in latest file
            List<string> storeEle = new List<string>();

            for (int row = 1; row < lines.Count(); row++)
            {
                storeEle.Add(lines[row]);
            }

            System.IO.File.WriteAllText(@filename2, string.Empty); // clear latest file
            addRecord(item, filename2); // add new data of item
            foreach (var ele in storeEle)
            {
                addRecord(ele, filename2); // remain the rest to be same
            }

            string skin_num = item.Substring(Math.Max(0, item.Length - 1));
            telltext.GetComponent<Text>().text = "Equip Skin " + skin_num + "!";
            Debug.Log("equip");
        }

        else
        {

            string[] itemLists = System.IO.File.ReadAllLines(@filename3); // take output from csv item lists
            string listPos_str = item.Substring(Math.Max(0, item.Length - 1)); // check item "1" <- last num
            int listPos = Int16.Parse(listPos_str) - 1; // take position
            string price_str = itemLists[listPos]; // get price from csv given by th position of index csv
            int price = Int16.Parse(price_str); // convert string (of price) into int

            if (real_money >= price) 
            {

                if (listPos < 3)
                {
                    addRecord(item, filename1); // add wepon
                    telltext.GetComponent<Text>().text = "Successfully Buy! (Click again to equip)";

                    // money left
                    real_money -= price;
                    updateMoney(real_money, moneyFile);
                }

                else // buy bomb
                {
                    string[] lines = System.IO.File.ReadAllLines(@filename2); // read data from latest (for getting old number of bombs)
                    string bombAmount = lines[listPos - 2]; // get bomb number in latest given by index from last letter from name such as, item "4" - 2 (take wepon index-1 out)
                    int bombAmount_int = Int16.Parse(bombAmount); // convert old bomb number
                    
                    if (bombAmount_int >= 3)
                    {
                        telltext.GetComponent<Text>().text = "Cannot Buy! (Maximun is 3)";
                    }

                    else
                    {
                        bombAmount_int += 1; // add one to previous

                        Debug.Log("number of bomb: " + bombAmount_int); // print number of bomb
                        Debug.Log("lines: " + lines.Count());

                        List<string> store = new List<string>();
                        for (int row = 0; row < lines.Count(); row++) // add new bomb number into correct lines in csv2
                        {
                            if (row == listPos - 2)
                            {
                                store.Add(bombAmount_int.ToString());
                            }
                            else
                            {
                                store.Add(lines[row]);
                            }
                        }

                        System.IO.File.WriteAllText(@filename2, string.Empty);

                        foreach (string itemInstore in store)
                        {
                            Debug.Log("add: " + itemInstore);
                            addRecord(itemInstore, filename2);
                        }

                        telltext.GetComponent<Text>().text = "Successfully Buy! (Bomb Amount: " + bombAmount_int.ToString() + ")";
                        
                        // money left
                        real_money -= price;
                        updateMoney(real_money, moneyFile);
                    }
                }   
            }

            else
            {
                telltext.GetComponent<Text>().text = "Not Enough Money!";
                Debug.Log("cannot buy");
            }
        }

    }

    public static void updateMoney(int money, string filepath)
    {
        System.IO.File.WriteAllText(@filepath, string.Empty);
        try
        {

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@filepath, true))
            {
                file.WriteLine(money);
            }
        }

        catch (Exception ex)
        {
            throw new ApplicationException("This program did an oopsie :", ex);
        }
    }

    public static void addRecord(string item, string filepath)
    {
        try
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@filepath, true))
            {
                file.WriteLine(item);
            }
        }

        catch(Exception ex)
        {
            throw new ApplicationException("This program did an oopsie :", ex);
        }

    }

    public static bool readRecord(string searchTerm, string filepath)
    {

        try
        {
            string[] lines = System.IO.File.ReadAllLines(@filepath);

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Equals(searchTerm))
                {
                    //Debug.Log("Found " + searchTerm.ToString());
                    return true;
                }
            }
            //Debug.Log("Not Found");
            return false;
        }

        catch(Exception ex)
        {
            return false;
            throw new ApplicationException("This program did an oopsie :", ex);
        }
    }

    
}
