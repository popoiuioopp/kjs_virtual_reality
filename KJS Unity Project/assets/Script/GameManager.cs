﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.EventSystems;
using System.Xml.Schema;
using Random = UnityEngine.Random;
using UnityEditor;
using System.Globalization;

public class GameManager : MonoBehaviour
{
    public GameObject enemy_1_object = null;
    public GameObject enemy_2_object = null;
    public GameObject enemy_3_object = null;
    public Transform spawnPoint1;
    public Transform spawnPoint2;
    public Transform spawnPoint3;

    public GameObject grenade = null;
    public GameObject banana = null;
    
    public Transform spawnPoint1_grenade;
    public Transform spawnPoint2_grenade;
    public Transform spawnPoint3_grenade;

    public Transform spawnPoint4_grenade;
    public Transform spawnPoint5_grenade;
    public Transform spawnPoint6_grenade;
   

    public float timeBetweenWaves = 5;
    private float countdownTime = 2f;
    public int waveNumber = 0;

    public static int maxHealth = 100;
    public static int health;
    public HealthBar healthBar;
    public StreakUI StreakBar;
    public Text scoreUi;
    public Text waveIncoming;
    public Text countdown;
    public static int score = 0;
    public static int streak = 0;
    public static int local_gold = 0;
    public static float streak_time = 0;
    public static bool gameover = false;

    public Text gold;

    Animator anim;

    public Leaderboard leaderboard;

    [SerializeField]
    private GameObject waveUI;
    [SerializeField]
    private GameObject gameOverUI;
    [SerializeField]
    private GameObject moneyUI;
    [SerializeField]
    private GameObject killUI;

    public int choc_bomb = 0;
    public int banana_bomb = 0;

    private void Start()
    {
        score = 0;
        //leaderboard = GameObject.FindObjectsOfType<GameManager>();
        anim = GetComponent<Animator>();
        scoreUi.text = "SCORE : " + score;
        health = maxHealth;
        StreakBar.SetMaxTime(5f);
        healthBar.SetMaxHealth(maxHealth);
        healthBar.SetHealth(health);
        gameover = false;

        // declare bomb's number
        string Weapon_filepath = "Assets/CSV Files/Latest.csv";

        try
        {
            string[] weapon_lists = System.IO.File.ReadAllLines(@Weapon_filepath);
            string skin = weapon_lists[0];
            string choc_bomb_str = weapon_lists[1];
            string banana_bombs_str = weapon_lists[2];

            choc_bomb = Int16.Parse(choc_bomb_str);
            banana_bomb = Int16.Parse(banana_bombs_str);
        }

        catch (Exception ex)
        {
            throw new ApplicationException("This program did an oopsie :", ex);
        }

        //Debug.Log("choc num: " + choc_bomb);
        //Debug.Log("bana num: " + banana_bomb);

        if (choc_bomb != 0)
        {
            Debug.Log("chohc num: " + choc_bomb);
            // Generate Choc Bomb
            if (choc_bomb == 1)
            {
                Instantiate(grenade, spawnPoint1_grenade.position, spawnPoint1_grenade.rotation);
            }
            else if(choc_bomb == 2)
            {
                Instantiate(grenade, spawnPoint1_grenade.position, spawnPoint1_grenade.rotation);
                Instantiate(grenade, spawnPoint2_grenade.position, spawnPoint2_grenade.rotation);
            }
            else if(choc_bomb == 3)
            {
                Instantiate(grenade, spawnPoint1_grenade.position, spawnPoint1_grenade.rotation);
                Instantiate(grenade, spawnPoint2_grenade.position, spawnPoint2_grenade.rotation);
                Instantiate(grenade, spawnPoint3_grenade.position, spawnPoint3_grenade.rotation);
            }
            
        }

        if (banana_bomb != 0)
        {
            Debug.Log("Bana num: " + banana_bomb);
            // Generate Banana bomb
            if (banana_bomb == 1)
            {
                Instantiate(banana, spawnPoint4_grenade.position, spawnPoint4_grenade.rotation);
            }
            else if (banana_bomb == 2)
            {
                Instantiate(banana, spawnPoint4_grenade.position, spawnPoint4_grenade.rotation);
                Instantiate(banana, spawnPoint5_grenade.position, spawnPoint5_grenade.rotation);
            }
            else if (banana_bomb == 3)
            {
                Instantiate(banana, spawnPoint4_grenade.position, spawnPoint4_grenade.rotation);
                Instantiate(banana, spawnPoint4_grenade.position, spawnPoint4_grenade.rotation);
                Instantiate(banana, spawnPoint6_grenade.position, spawnPoint6_grenade.rotation);
            }
        }
    }


    void Update()
    {
        scoreUi.text = "SCORE : " + score;
        waveIncoming.text = "WAVE " + Mathf.Round(waveNumber) + " INCOMING";
        countdown.text = "" + Mathf.Round(countdownTime);
        healthBar.SetHealth(health);
        StreakBar.SetTime(streak_time);
        gold.text = "" + local_gold;

        if (streak_time > 0)
        {
            killUI.SetActive(true);
            streak_time -= Time.deltaTime;
        }

        if (streak_time < 0)
        {
            streak = 0;
            streak_time = 0f;
            killUI.SetActive(false);
        }

        if (health < 1 && !gameover) //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// DEADDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        {
            FindObjectOfType<AudioManager>().Play("GameOver");
            gameover = true;
            getName(); // Add score
            //leaderboard.AddHighscoreEntry(score, "DEAR");
            gameOverUI.SetActive(true);
            StartCoroutine(goldUIFade());
            StartCoroutine(EndWait());
            local_gold = (score / 3000);
            clearBomb();
            addMoney(local_gold);
            FindObjectOfType<AudioManager>().Play("GoldGain");
        }

        if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 0 && health > 0 && !gameover)
        {
            if (waveNumber > 0)
            {
                waveUI.SetActive(true);
                FindObjectOfType<AudioManager>().Play("WaveIncoming");
            }
            if (countdownTime <= 0f)
            {
                if (waveNumber > 0)
                {
                    StartCoroutine(waveUIFade());
                }
                StartCoroutine(SpawnWave());
                countdownTime = timeBetweenWaves;
            }

            countdownTime -= Time.deltaTime;
        }
    }

    public static void addMoney(int money)
    {

        string filepath = "Assets/CSV Files/Money.csv";

        try
        {
            string[] old_money_str = System.IO.File.ReadAllLines(@filepath);
            Debug.Log(old_money_str[0]);
            int old_money = Int16.Parse(old_money_str[0]);
            Debug.Log(old_money);
            int total_money = old_money + money;

            System.IO.File.WriteAllText(@filepath, string.Empty);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@filepath, true))
            {
                file.WriteLine(total_money);
            }
        }

        catch (Exception ex)
        {
            throw new ApplicationException("This program did an oopsie :", ex);
        }

    }

    public void getName()
    {
        string filepath = "Assets/CSV Files/Playername.csv";

        try
        {
            string[] nameList = System.IO.File.ReadAllLines(@filepath);
            leaderboard.AddHighscoreEntry(score, nameList[0]);
        }

        catch (Exception ex)
        {
            leaderboard.AddHighscoreEntry(score, "User1");
            throw new ApplicationException("This program did an oopsie :", ex);
        }
    }

    public static void clearBomb()
    {

        string filepath = "Assets/CSV Files/Latest.csv";

        try
        {
            string[] old_latest = System.IO.File.ReadAllLines(@filepath);
            string last_skin = old_latest[0];
            System.IO.File.WriteAllText(@filepath, string.Empty);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@filepath, true))
            {
                file.WriteLine(last_skin);

                for (int i = 0; i < 2; i++)
                {
                    file.WriteLine("1");
                }
            }
        }

        catch (Exception ex)
        {
            throw new ApplicationException("This program did an oopsie :", ex);
        }

    }

    IEnumerator EndWait()
    {
        yield return new WaitForSeconds(7);
        // addMoney(local_gold);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    IEnumerator waveUIFade()
    {
        anim.SetBool("WaveOut", true);
        yield return new WaitForSeconds(0f);
        waveUI.SetActive(false);
    }
    IEnumerator goldUIFade()
    {
        yield return new WaitForSeconds(1.5f);
        moneyUI.SetActive(true);
        yield return new WaitForSeconds(3f);
        moneyUI.SetActive(false);
    }

    IEnumerator SpawnWave()
    {
        float spawning_delay;
        if (waveNumber == 1) {
            spawning_delay = Random.Range(0.6f, 1f);
            for (int i = 0; i < 5; i++)
            {
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 2)
        {
            for (int i = 0; i < 6; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 1; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 3)
        {
            for (int i = 0; i < 7; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 2; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 4)
        {
            for (int i = 0; i < 6; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 2; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 1; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_3_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 5)
        {
            for (int i = 0; i < 4; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 3; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 2; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_3_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 6)
        {
            for (int i = 0; i < 3; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 4; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 2; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_3_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 7)
        {
            for (int i = 0; i < 3; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 3; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 3; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_3_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 8)
        {
            for (int i = 0; i < 2; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 4; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 3; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_3_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 9)
        {
            for (int i = 0; i < 2; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 3; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 4; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_3_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        if (waveNumber == 10)
        {
            for (int i = 0; i < 1; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_1_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 4; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_2_object);
                yield return new WaitForSeconds(spawning_delay);
            }
            for (int i = 0; i < 4; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                SpawnEnemy(enemy_3_object);
                yield return new WaitForSeconds(spawning_delay);
            }
        }
        else if (waveNumber>10)
        {
            for (int i = 0; i < waveNumber; i++)
            {
                spawning_delay = Random.Range(0.6f, 1f);
                int spawning_type = Random.Range(1, 4);
                if (Mathf.Round(spawning_type) == 1)
                {
                    SpawnEnemy(enemy_1_object);
                    yield return new WaitForSeconds(spawning_delay);

                }
                else if (Mathf.Round(spawning_type) == 2)
                {
                    SpawnEnemy(enemy_2_object);
                    yield return new WaitForSeconds(spawning_delay);
                }
                else if (Mathf.Round(spawning_type) == 3)
                {
                    SpawnEnemy(enemy_3_object);
                    yield return new WaitForSeconds(spawning_delay);
                }
              
            }
        }
        waveNumber++;
    }

    void SpawnEnemy(GameObject enemy)
    {
        int spawning_point = Random.Range(1, 4);

        if(Mathf.Round(spawning_point) == 1)
        {
            Instantiate(enemy, spawnPoint1.position, spawnPoint1.rotation);
            
        }
        else if(Mathf.Round(spawning_point) == 2)
        {
            Instantiate(enemy, spawnPoint2.position, spawnPoint2.rotation);
        }
        else if (Mathf.Round(spawning_point) == 3)
        {
            Instantiate(enemy, spawnPoint3.position, spawnPoint3.rotation);
        }
    }

}
