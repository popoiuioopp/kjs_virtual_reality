﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchShop : MonoBehaviour
{

    public GameObject ShopMenu;
    public GameObject StartMenu;

    public bool _shop = false;

    public void Shopping()
    {
        if (_shop)
        {

            StartMenu.SetActive(true);
            ShopMenu.SetActive(false);
            _shop = !_shop;

        }
        else if (!_shop)
        {

            StartMenu.SetActive(false);
            ShopMenu.SetActive(true);
            _shop = !_shop;

        }
    }
}
