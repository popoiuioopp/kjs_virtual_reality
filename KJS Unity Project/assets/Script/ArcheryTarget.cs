﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Target that sends events when hit by an arrow
//
//=============================================================================

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Security.Cryptography;
using UnityEngine.SceneManagement;

namespace Valve.VR.InteractionSystem
{
	//-------------------------------------------------------------------------
	public class ArcheryTarget : MonoBehaviour
	{
		public UnityEvent onTakeDamage;

		public bool onceOnly = false;
		public Transform targetCenter;
		
		public Transform baseTransform;
		public Transform fallenDownTransform;
		public float fallTime = 0.5f;

		const float targetRadius = 0.25f;

		private bool targetEnabled = true;

		public GameObject impactEffect;

		public static int maxTimer = 5;
		public static int Timer;

		public static int HP_enemy3 = 2;

		public BalloonSpawner balloonSpawner;

		//-------------------------------------------------
		private void ApplyDamage()
		{
			OnDamageTaken();
		}


		//-------------------------------------------------
		private void FireExposure()
		{
			OnDamageTaken();
		}

		public void Destroy_hit()
		{
			GameObject effectIns = Instantiate(impactEffect, transform.position, transform.rotation);
			Destroy(effectIns, 2.0f);
			Destroy(gameObject);
			FindObjectOfType<AudioManager>().Play("AntDead");
			if (gameObject.tag == "Enemy" && !GameManager.gameover)
			{
				GameManager.score += 50 * (GameManager.streak + 1);
			}
		}
		public void Destroy_hit_enemy3()
		{
			Debug.Log("HITTTTTTTTTTTTTTT");
			Debug.Log("HP" + HP_enemy3);
			HP_enemy3 -= 1;
			Debug.Log("HP" + HP_enemy3);
			if (HP_enemy3 % 2 == 0)
			{
				GameObject effectIns = Instantiate(impactEffect, transform.position, transform.rotation);
				Destroy(effectIns, 2.0f);
				Destroy(gameObject);
				balloonSpawner.SpawnBalloonFromEvent(14);
				FindObjectOfType<AudioManager>().Play("AntDead");
			}
			if (gameObject.tag == "Enemy")
			{
				GameManager.score += 50 * (GameManager.streak + 1);
			}
		}

		public void Destroy_enemy2()
		{
			GameObject effectIns = Instantiate(impactEffect, transform.position, transform.rotation);
			Destroy(effectIns, 2.0f);
			Destroy(transform.parent.gameObject);
			FindObjectOfType<AudioManager>().Play("AntDead");
			if (!GameManager.gameover)
			{
				GameManager.score += 300 * (GameManager.streak + 1);
				if (GameManager.streak < 6)
				{
					if (GameManager.streak == 1) { FindObjectOfType<AudioManager>().Play("Combo1"); }
					if (GameManager.streak == 2) { FindObjectOfType<AudioManager>().Play("Combo2"); }
					if (GameManager.streak == 3) { FindObjectOfType<AudioManager>().Play("Combo3"); }
					if (GameManager.streak == 4) { FindObjectOfType<AudioManager>().Play("Combo4"); }
					if (GameManager.streak == 5) { FindObjectOfType<AudioManager>().Play("Combo5"); }
					GameManager.streak += 1;
					GameManager.streak_time = 5;
				}
			}
		}


		//-------------------------------------------------
		private void OnDamageTaken()
		{
			if ( targetEnabled )
			{
				onTakeDamage.Invoke();
				StartCoroutine( this.FallDown() );

				if ( onceOnly )
				{
					targetEnabled = false;
				}
			}
		}


		//-------------------------------------------------
		private IEnumerator FallDown()
		{
			if ( baseTransform )
			{
				Quaternion startingRot = baseTransform.rotation;

				float startTime = Time.time;
				float rotLerp = 0f;

				while ( rotLerp < 1 )
				{
					rotLerp = Util.RemapNumberClamped( Time.time, startTime, startTime + fallTime, 0f, 1f );
					baseTransform.rotation = Quaternion.Lerp( startingRot, fallenDownTransform.rotation, rotLerp );
					yield return null;
				}
			}

			yield return null;
		}
		public void HitHead()
		{
			Debug.Log("HitHead");
		}

		public void LoadingMenu()
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
		}

		public void LoadingMenuFromMain()
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
		}

		public void HitBody()
		{
			Debug.Log("HitBody");
		}
	}
}
