﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopMenu : MonoBehaviour
{

    public Material MaterialArrow1;
    public Material MaterialArrow2;
    public GameObject Object;

    public void button1_Trig()
    {
        Object.GetComponent<MeshRenderer>().material = MaterialArrow1;
    }

    public void button2_Trig()
    {
        Object.GetComponent<MeshRenderer>().material = MaterialArrow2;
    }

}
