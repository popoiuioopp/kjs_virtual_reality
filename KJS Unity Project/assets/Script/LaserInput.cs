﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.Extras;
using Valve.VR.InteractionSystem;
//using System.Diagnostics;

public class LaserInput : MonoBehaviour
{
    public static GameObject currentObject;
    int currentID;
    public bool click = false;

    public MainMenu mainFunc;
    public switchShop switchShopFunc;
    public ShopManager shopMan;
    public Keyboard typeKey;

    public SteamVR_Action_Boolean TriggerClick;
    private SteamVR_Input_Sources inputSource;

    //List<String> shopitems = new List<String>();

    private void OnEnable()
    {
        TriggerClick.AddOnStateDownListener(Press, inputSource);
    }

    private void OnDisable()
    {
        TriggerClick.RemoveOnStateDownListener(Press, inputSource);
    }

    private void Press(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        click = true;
    }

    void Start()
    {
        currentObject = null;
        currentID = 0;

        /*
        // Add Items
        shopitems.Add("Item 1");
        shopitems.Add("Item 2");
        shopitems.Add("Item 3");
        shopitems.Add("Item 4");
        shopitems.Add("Item 5");
        */
    }

    void Update()
    {
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, transform.forward, 100.0f);

        for (int i = 0; i < hits.Length; i++)
        {
            RaycastHit hit = hits[i];

            int id = hit.collider.gameObject.GetInstanceID();

            if (currentID != id)
            {
                currentID = id;
                currentObject = hit.collider.gameObject;
                string tag = currentObject.tag;

                string name = currentObject.name;
                
                if (name == "Start" && click)
                {
                    mainFunc.PlayGame();
                
                } else if (name == "Training" && click)
                {
                    mainFunc.TutorialMode();

                } else if (name == "Exit" && click)
                {
                    mainFunc.QuitGame();
                
                } else if ((name == "Return" || name == "Shop") && click)
                {
                    switchShopFunc.Shopping();

                } else if (tag == "ItemsInShop" && click)
                {
                    shopMan.OnClicked(name);

                } else if (tag == "TextInKeyboard" && click) // below condition is text algo
                {
                    typeKey.alphabetFunction(name);

                } else if (tag == "deleteButton" && click)
                {
                    typeKey.deleteFunction();

                }else if (tag == "capButton" && click)
                {
                    typeKey.capFunction();

                } else if (tag == "spaceButton" && click)
                {
                    typeKey.alphabetFunction(" ");

                } else if (tag == "enterButton" && click)
                {
                    typeKey.enterFunction();

                } else if (tag == "askButton" && click)
                {
                    if (name == "No")
                    {
                        Debug.Log("Reach");
                        typeKey.introSwitch();
                    }
                    else
                    {
                        typeKey.noChange();
                    }

                } else if (tag == "returnToIntro" && click) {

                    typeKey.introSwitch();

                } else if (name == "Reset" && click)
                {

                    shopMan.clearFile();

                }



                /*
                string tag = currentObject.tag;
                if(tag == "Button")
                {
                    Debug.Log("HIT Button");
                }
                */
            }
        }

        click = false;
    }
}
